package com.example.tmstechnicaltest.controller;

import com.example.tmstechnicaltest.request.AESEncryptRequestData;
import com.example.tmstechnicaltest.response.AESEncryptResponseData;
import com.example.tmstechnicaltest.service.AESEncryptDecryptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AESEncryptController {

    @Autowired
    AESEncryptDecryptService aesEncryptDecryptService;

    @PostMapping("/aes/encrypt")
    public AESEncryptResponseData encryptText(@RequestBody AESEncryptRequestData aesEncryptRequestData)
            throws RuntimeException {
        return aesEncryptDecryptService.AESEncrypt(aesEncryptRequestData.getPlain_text(), aesEncryptRequestData.getAes_key());
    }
}
