package com.example.tmstechnicaltest.controller;

import com.example.tmstechnicaltest.request.AESDecryptRequestData;
import com.example.tmstechnicaltest.response.AESDecryptResponseData;
import com.example.tmstechnicaltest.service.AESEncryptDecryptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AESDecryptController {

    @Autowired
    AESEncryptDecryptService aesEncryptDecryptService;

    @PostMapping("/aes/decrypt")
    public AESDecryptResponseData decryptText(@RequestBody AESDecryptRequestData aesDecryptRequestData)
            throws RuntimeException {
        return aesEncryptDecryptService.AESDecrypt(aesDecryptRequestData.getCipher_text(), aesDecryptRequestData.getAes_key());
    }
}
