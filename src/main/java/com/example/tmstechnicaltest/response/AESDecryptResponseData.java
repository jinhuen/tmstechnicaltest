package com.example.tmstechnicaltest.response;

import lombok.Data;

@Data
public class AESDecryptResponseData {

    private String plain_text;

    private String base64;
}
