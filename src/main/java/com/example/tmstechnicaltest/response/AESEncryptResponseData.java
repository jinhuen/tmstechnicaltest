package com.example.tmstechnicaltest.response;

import lombok.Data;

@Data
public class AESEncryptResponseData {

    private String cipher_text;

}
