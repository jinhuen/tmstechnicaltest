package com.example.tmstechnicaltest.service.Impl;

import com.example.tmstechnicaltest.response.AESDecryptResponseData;
import com.example.tmstechnicaltest.response.AESEncryptResponseData;
import com.example.tmstechnicaltest.service.AESEncryptDecryptService;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;

@Service
public class AESEncryptDecryptServiceImpl implements AESEncryptDecryptService {

    @Override
    public AESEncryptResponseData AESEncrypt(String plainText, String aesKey) {
        try {
            SecretKeySpec secretKey = new SecretKeySpec(aesKey.getBytes(), "AES");

            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            AESEncryptResponseData aesEncryptResponseData = new AESEncryptResponseData();
            aesEncryptResponseData.setCipher_text(Base64.encodeBase64String(cipher.doFinal(plainText.getBytes(StandardCharsets.UTF_8))));
            return aesEncryptResponseData;
        } catch (Exception e) {
            System.out.println("Error while encrypting: " + e);
        }
        return null;
    }

    @Override
    public AESDecryptResponseData AESDecrypt(String cipherText, String aesKey) {
        try {
            SecretKeySpec secretKey = new SecretKeySpec(aesKey.getBytes(), "AES");

            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            AESDecryptResponseData aesDecryptResponseData = new AESDecryptResponseData();
            aesDecryptResponseData.setPlain_text(new String(cipher.doFinal(Base64.decodeBase64(cipherText))));
            aesDecryptResponseData.setBase64(Base64.encodeBase64String(cipher.doFinal(Base64.decodeBase64(cipherText))));
            return aesDecryptResponseData;
        } catch (Exception e) {
            System.out.println("Error while decrypting: " + e);
        }
        return null;
    }
}
