package com.example.tmstechnicaltest.service;

import com.example.tmstechnicaltest.response.AESDecryptResponseData;
import com.example.tmstechnicaltest.response.AESEncryptResponseData;

public interface AESEncryptDecryptService {

    AESEncryptResponseData AESEncrypt(String plainText, String aesKey);

    AESDecryptResponseData AESDecrypt(String cipherText, String aesKey);
}
