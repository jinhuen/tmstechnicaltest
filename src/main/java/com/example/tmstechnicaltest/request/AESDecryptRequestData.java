package com.example.tmstechnicaltest.request;

import lombok.Data;

@Data
public class AESDecryptRequestData {

    private String cipher_text;

    private String aes_key;
}
