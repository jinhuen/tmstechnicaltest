package com.example.tmstechnicaltest.request;

import lombok.Data;

@Data
public class AESEncryptRequestData {

    private String plain_text;

    private String aes_key;

}
