package com.example.tmstechnicaltest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TmsTechnicalTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(TmsTechnicalTestApplication.class, args);
    }

}
