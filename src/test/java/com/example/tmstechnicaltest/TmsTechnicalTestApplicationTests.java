package com.example.tmstechnicaltest;

import com.example.tmstechnicaltest.service.AESEncryptDecryptService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Objects;

@SpringBootTest
class TmsTechnicalTestApplicationTests {

    @Autowired
    AESEncryptDecryptService aesEncryptDecryptService;

    @Test
    void testEncrypt() {
        String samplePlainText = "Apple";
        String aesKey = "404D635166546A576E5A723475377721";
        String encryptedText = Objects.requireNonNull(aesEncryptDecryptService.AESEncrypt(samplePlainText, aesKey)).getCipher_text();
        String decryptedText = Objects.requireNonNull(aesEncryptDecryptService.AESDecrypt(encryptedText, aesKey)).getPlain_text();
        Assertions.assertEquals(samplePlainText, decryptedText);
    }

    @Test
    void testDecrypt() {
        String sampleCipherText = "yeRh6A7DBHlErK6WqYlrww==";
        String aesKey = "404D635166546A576E5A723475377721";
        String decryptedText = Objects.requireNonNull(aesEncryptDecryptService.AESDecrypt(sampleCipherText, aesKey)).getPlain_text();
        String encryptedText = Objects.requireNonNull(aesEncryptDecryptService.AESEncrypt(decryptedText, aesKey)).getCipher_text();
        Assertions.assertEquals(sampleCipherText, encryptedText);
    }

    @Test
    void testEncrypt_withDifferentKey() {
        String samplePlainText = "Apple";
        String aesKeyOriginal = "404D635166546A576E5A723475377721";
        String aesKeyNew = "68566D597133743677397A2443264629";
        String encryptedTextOriginal = Objects.requireNonNull(aesEncryptDecryptService.AESEncrypt(samplePlainText, aesKeyOriginal)).getCipher_text();
        String encryptedTextNew = Objects.requireNonNull(aesEncryptDecryptService.AESEncrypt(samplePlainText, aesKeyNew)).getCipher_text();
        Assertions.assertNotEquals(encryptedTextOriginal, encryptedTextNew);
    }

}
